package com.example.keycloakserver.model;

import lombok.Data;

@Data
public class SenderResponse {
    private String message;

    public SenderResponse(String message) {
        this.message = message;
    }
}
