import { Component } from '@angular/core';
import {KeycloakService} from "keycloak-angular";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'keycloack-client';

  constructor(private keycloackService: KeycloakService) {}
  logout() {
    this.keycloackService.logout();
  }
}
